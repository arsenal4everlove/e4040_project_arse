"""
Source Code for Final Project of ECBM E4040, Fall 2016, Columbia University

Instructor: Prof. Zoran Kostic

This code is based on
[1] http://deeplearning.net/tutorial/logreg.html
[2] http://deeplearning.net/tutorial/mlp.html
[3] http://deeplearning.net/tutorial/lenet.html
"""


from __future__ import print_function
from six.moves import cPickle
from numpy import linalg as LA
from theano.tensor.nnet.abstract_conv import bilinear_upsampling
import inspect
import numpy
import sys
import random
from scipy import ndimage
import theano
import timeit
import theano.tensor as T
from theano.tensor.signal import downsample
from theano.tensor.signal import pool
from theano.tensor.nnet import conv2d
import math

from hw3_utils import shared_dataset, load_data
from hw3_nn import LogisticRegression, LogisticRegression_2, HiddenLayer, LeNetConvPoolLayer, train_nn

class MaxoutLayer(object):
    def __init__(self, rng, input, n_in, n_out, W=None, b=None,
                maxoutsize = 5):
        """
        Hidden unit activation is given by: activation(dot(input,W) + b)

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights
        
        :type is_train: theano.iscalar   
        :param is_train: indicator pseudo-boolean (int) for switching between training and prediction

        :type input: theano.tensor.dmatrix
        :param input: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        :type activation: theano.Op or function
        :param activation: Non linearity to be applied in the hidden
                           layer
                           
        :type p: float or double
        :param p: probability of NOT dropping out a unit   
        """
        self.input = input

        W = [0] * maxoutsize
        W_values = [0] * maxoutsize
        b_values = [0] * maxoutsize
        b = [0] * maxoutsize
        self.params = []
        for i in range(maxoutsize):
            W_values[i] = numpy.asarray(
                rng.uniform(
                    low=-numpy.sqrt(6. / (n_in + n_out)),
                    high=numpy.sqrt(6. / (n_in + n_out)),
                    size=(n_in, n_out)
                ),
                dtype=theano.config.floatX
            )

            W[i] = theano.shared(value=W_values[i], borrow=True)
            self.params.append(W[i])
                
            b_values[i] = numpy.zeros((n_out,), dtype=theano.config.floatX)
            b[i] = theano.shared(value=b_values[i], borrow=True)
            self.params.append(b[i])

        self.W = W
        self.b = b
        maxout_out = T.dot(input, self.W[0]) + self.b[0]
        for i in range(maxoutsize - 1):
            temp = T.dot(input, self.W[i + 1]) + self.b[i + 1]
            maxout_out = T.maximum(maxout_out,temp)

        
        self.output = maxout_out

        ## self.L2_sqr = (self.W ** 2).sum()

class MyLeNetConvLayer(object):
    """Pool Layer of a convolutional network """

    def __init__(self, rng, input, filter_shape, image_shape,border_mode = 'half'):
        """
        Allocate a LeNetConvPoolLayer with shared variable internal parameters.

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dtensor4
        :param input: symbolic image tensor, of shape image_shape

        :type filter_shape: tuple or list of length 4
        :param filter_shape: (number of filters, num input feature maps,
                              filter height, filter width)

        :type image_shape: tuple or list of length 4
        :param image_shape: (batch size, num input feature maps,
                             image height, image width)

        :type poolsize: tuple or list of length 2
        :param poolsize: the downsampling (pooling) factor (#rows, #cols)
        """

        assert image_shape[1] == filter_shape[1]
        self.input = input

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = numpy.prod(filter_shape[1:])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]))
        # initialize weights with random weights
        W_bound = numpy.sqrt(6. / (fan_in + fan_out))
        self.W = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        # convolve input feature maps with filters
        conv_out = conv2d(
            input=input,
            filters=self.W,
            filter_shape=filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )
        

        self.W2 = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )
        self.output = T.nnet.relu(conv_out + self.b.dimshuffle('x', 0, 'x', 'x'))
        #self.output = T.tanh(conv_out)
        # store parameters of this layer
        self.params = [self.W, self.b]

        # keep track of model input
        self.input = input
        self.L2_sqr = (self.W ** 2).sum()

class LeNetMaxConvLayer(object):
    """Pool Layer of a convolutional network """

    def __init__(self, rng, input, filter_shape, image_shape,border_mode = 'half'):
        """
        Allocate a LeNetConvPoolLayer with shared variable internal parameters.

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dtensor4
        :param input: symbolic image tensor, of shape image_shape

        :type filter_shape: tuple or list of length 4
        :param filter_shape: (number of filters, num input feature maps,
                              filter height, filter width)

        :type image_shape: tuple or list of length 4
        :param image_shape: (batch size, num input feature maps,
                             image height, image width)

        :type poolsize: tuple or list of length 2
        :param poolsize: the downsampling (pooling) factor (#rows, #cols)
        """

        assert image_shape[1] == filter_shape[1]
        self.input = input

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = numpy.prod(filter_shape[1:])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]))
        # initialize weights with random weights
        W_bound = numpy.sqrt(6. / (fan_in + fan_out))
        self.W = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        self.W2 = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values_2 = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b2 = theano.shared(value=b_values_2, borrow=True)

        # convolve input feature maps with filters
        conv_out = conv2d(
            input=input,
            filters=self.W,
            filter_shape= filter_shape,
            input_shape=image_shape,
        )

        conv_out2 = conv2d(
            input=input,
            filters=self.W2,
            filter_shape= filter_shape,
            input_shape=image_shape,
        )
        
        maxout_out = T.maximum(conv_out + self.b.dimshuffle('x', 0, 'x', 'x'), conv_out2 + self.b2.dimshuffle('x', 0, 'x', 'x')) 
        #self.output = T.tanh(conv_out)
        # store parameters of this layer
        self.params = [self.W, self.b,self.W2, self.b2]
        self.output = maxout_out
        # keep track of model input
        self.input = input
        self.L2_sqr = (self.W ** 2).sum() + (self.W2 ** 2).sum()        



class ConvMaxLayer(object):
    """Pool Layer of a convolutional network """

    def __init__(self, rng, input, filter_shape, image_shape, poolsize = (4,4), st = (2,2), border_mode = 'half'):
        """
        Allocate a LeNetConvPoolLayer with shared variable internal parameters.

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dtensor4
        :param input: symbolic image tensor, of shape image_shape

        :type filter_shape: tuple or list of length 4
        :param filter_shape: (number of filters, num input feature maps,
                              filter height, filter width)

        :type image_shape: tuple or list of length 4
        :param image_shape: (batch size, num input feature maps,
                             image height, image width)

        :type poolsize: tuple or list of length 2
        :param poolsize: the downsampling (pooling) factor (#rows, #cols)
        """

        assert image_shape[1] == filter_shape[1]
        self.input = input

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = numpy.prod(filter_shape[1:])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]))
        # initialize weights with random weights
        W_bound = numpy.sqrt(6. / (fan_in + fan_out))
        self.W = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        self.W2 = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        self.Wt = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values_2 = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b2 = theano.shared(value=b_values_2, borrow=True)

        # convolve input feature maps with filters
        conv_out = conv2d(
            input=input,
            filters=self.W,
            filter_shape= filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )

        conv_out2 = conv2d(
            input=input,
            filters=self.W2,
            filter_shape= filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )

        if poolsize[0] == 4:
            padding = (1,1)
        else:
            padding = (0,0)
        maxout_out = T.maximum(conv_out + self.b.dimshuffle('x', 0, 'x', 'x'), conv_out2 + self.b2.dimshuffle('x', 0, 'x', 'x')) 
        pooled_out2 = pool.pool_2d(
            input=maxout_out,
            ds=poolsize,
            ignore_border=True,
            st = st,
            padding = padding,
        )
        #self.output = T.tanh(conv_out)
        # store parameters of this layer
        self.params = [self.W, self.b,self.W2, self.b2]
        self.output = pooled_out2
        # keep track of model input
        self.input = input
        self.L2_sqr = (self.W ** 2).sum() + (self.W2 ** 2).sum()


class ConvMaxLayer_nopooling(object):
    """Pool Layer of a convolutional network """

    def __init__(self, rng, input, filter_shape, image_shape, border_mode = 'half'):
        """
        Allocate a LeNetConvPoolLayer with shared variable internal parameters.

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dtensor4
        :param input: symbolic image tensor, of shape image_shape

        :type filter_shape: tuple or list of length 4
        :param filter_shape: (number of filters, num input feature maps,
                              filter height, filter width)

        :type image_shape: tuple or list of length 4
        :param image_shape: (batch size, num input feature maps,
                             image height, image width)

        :type poolsize: tuple or list of length 2
        :param poolsize: the downsampling (pooling) factor (#rows, #cols)
        """

        assert image_shape[1] == filter_shape[1]
        self.input = input

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = numpy.prod(filter_shape[1:])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]))
        # initialize weights with random weights
        W_bound = numpy.sqrt(6. / (fan_in + fan_out))
        self.W = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        self.W2 = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values_2 = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b2 = theano.shared(value=b_values_2, borrow=True)

        # convolve input feature maps with filters
        conv_out = conv2d(
            input=input,
            filters=self.W,
            filter_shape= filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )

        conv_out2 = conv2d(
            input=input,
            filters=self.W2,
            filter_shape= filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )
        
        maxout_out = T.maximum(conv_out + self.b.dimshuffle('x', 0, 'x', 'x'), conv_out2 + self.b2.dimshuffle('x', 0, 'x', 'x')) 
        #self.output = T.tanh(conv_out)
        # store parameters of this layer
        self.params = [self.W, self.b,self.W2, self.b2]
        self.output = maxout_out
        # keep track of model input
        self.input = input
        self.L2_sqr = (self.W ** 2).sum() + (self.W2 ** 2).sum()        



class ConvMaxLayer_nopooling_highway(object):
    """Pool Layer of a convolutional network """

    def __init__(self, rng, input, filter_shape, image_shape, border_mode = 'half', bias_init = -3):
        """
        Allocate a LeNetConvPoolLayer with shared variable internal parameters.

        :type rng: numpy.random.RandomState
        :param rng: a random number generator used to initialize weights

        :type input: theano.tensor.dtensor4
        :param input: symbolic image tensor, of shape image_shape

        :type filter_shape: tuple or list of length 4
        :param filter_shape: (number of filters, num input feature maps,
                              filter height, filter width)

        :type image_shape: tuple or list of length 4
        :param image_shape: (batch size, num input feature maps,
                             image height, image width)

        :type poolsize: tuple or list of length 2
        :param poolsize: the downsampling (pooling) factor (#rows, #cols)
        """

        assert image_shape[1] == filter_shape[1]
        self.input = input

        # there are "num input feature maps * filter height * filter width"
        # inputs to each hidden unit
        fan_in = numpy.prod(filter_shape[1:])
        # each unit in the lower layer receives a gradient from:
        # "num output feature maps * filter height * filter width" /
        #   pooling size
        fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]))
        # initialize weights with random weights
        W_bound = numpy.sqrt(6. / (fan_in + fan_out))
        self.W = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b = theano.shared(value=b_values, borrow=True)

        self.W2 = theano.shared(
            numpy.asarray(
                rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
                dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values_2 = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
        self.b2 = theano.shared(value=b_values_2, borrow=True)

        self.Wt = theano.shared(

            numpy.asarray(
            rng.normal(
                loc=0,
                scale=numpy.sqrt(2. / (fan_in)),
                size=filter_shape),
            dtype=theano.config.floatX
            ),
            borrow=True
        )

        # the bias is a 1D tensor -- one bias per output feature map
        b_values_t = numpy.ones((filter_shape[0],), dtype=theano.config.floatX) * bias_init
        self.b_t = theano.shared(value=b_values_t, borrow=True)

        # convolve input feature maps with filters
        conv_out = conv2d(
            input=input,
            filters=self.W,
            filter_shape= filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )

        conv_out2 = conv2d(
            input=input,
            filters=self.W2,
            filter_shape= filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )

        conv_out_t = conv2d(
            input=input,
            filters=self.Wt,
            filter_shape= filter_shape,
            input_shape=image_shape,
            border_mode = border_mode
        )
        maxout_out = T.maximum(conv_out + self.b.dimshuffle('x', 0, 'x', 'x'), conv_out2 + self.b2.dimshuffle('x', 0, 'x', 'x')) 

        transfrom_gate = T.nnet.sigmoid(conv_out_t + self.b_t.dimshuffle('x', 0, 'x', 'x'))

        self.output = transfrom_gate * maxout_out + (1 - transfrom_gate) * input
        #self.output = T.tanh(conv_out)
        # store parameters of this layer
        self.params = [self.W, self.b,self.W2, self.b2, self.Wt, self.b_t]
        
        # keep track of model input
        self.input = input
        self.L2_sqr = (self.W ** 2).sum() + (self.W2 ** 2).sum() + (self.Wt ** 2).sum()       

#Write a function to flip images
def flip_image(input_image):
    temp = numpy.fliplr(numpy.reshape(input_image,(3,32,32)).transpose(1,2,0))
    flipped_image = numpy.reshape(numpy.reshape(temp,(32,32,3)).transpose(2,0,1),3072)
    return flipped_image

#Write a function to add noise, it should at least provide Gaussian-distributed and uniform-distributed noise with zero mean
def noise_injection(input_image):
    x = random.randint(0,1)
    if x == 1:
        noise_data = numpy.copy(numpy.random.normal(0,0.02,(input_image.shape[0],input_image.shape[1])) + input_image)
    if x == 0:
        noise_data = numpy.copy(numpy.random.uniform(-0.02,0.02,(input_image.shape[0],input_image.shape[1])) + input_image)
    
    return noise_data
                       
#Implement the convolutional neural network depicted in problem4 

def RMSprop(params,grads, learning_rate=0.01, batch_size = 50,rho=0.9, epsilon=1e-6):
    updates = []
    for p, g in zip(params, grads):
        acc = theano.shared(p.get_value() * 0.)
        g = g / (batch_size * 1.0)
        acc_new = rho * acc + (1 - rho) * g ** 2
        gradient_scaling = T.sqrt(acc_new + epsilon)
        g = g / gradient_scaling
        updates.append((acc, acc_new))
        updates.append((p, p - learning_rate * g))
    return updates

def gradient_updates_momentum(params, cost, learning_rate = 0.0001, batch_size = 50, momentum = 0.5):
 
    momentum =theano.shared(numpy.cast[theano.config.floatX](momentum), name='momentum')
    updates = []
    for param in params:
        param_update = theano.shared(param.get_value()*numpy.cast[theano.config.floatX](0.))    
        updates.append((param, param - learning_rate*param_update))
        updates.append((param_update, momentum*param_update + (numpy.cast[theano.config.floatX](1.) - momentum)*T.grad(cost, param) / (batch_size * 1.)))
  
    return updates

def Adam(params,grads, learning_rate=0.0002, b1=0.1, b2=0.001, e=1e-8):
    updates = []
    i = theano.shared(numpy.asarray(0.,dtype = theano.config.floatX))
    i = i + 1.
    i_t = i
    #print (i)
    fix1 = 1. - (1. - b1)**i_t
    fix2 = 1. - (1. - b2)**i_t
    lr_t = learning_rate * (T.sqrt(fix2) / fix1)
    for p, g in zip(params, grads):
        m = theano.shared(p.get_value() * 0.)
        v = theano.shared(p.get_value() * 0.)
        m_t = (b1 * g) + ((1. - b1) * m)
        v_t = (b2 * T.sqr(g)) + ((1. - b2) * v)
        g_t = m_t / (T.sqrt(v_t) + e)
        p_t = p - (lr_t * g_t)
        updates.append((m, m_t))
        updates.append((v, v_t))
        updates.append((p, p_t))
        #updates.append((i, i_t))
    return updates

#Implement a fitnet neural network to achieve at least 80% testing accuracy on CIFAR-dataset
def MY_fitnet(datasets, learning_rate=0.00005, n_epochs=128, batch_size = 32,verbose=True,decay_rate = 0.9, L2_rate = 0.00002):
    
    rng = numpy.random.RandomState(23455) 
    train_set = shared_dataset(datasets[0])
    valid_set = shared_dataset(datasets[1])
    test_set = shared_dataset(datasets[2])
    
    
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    # start-snippet-1
    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (28-5+1 , 28-5+1) = (24, 24)
    # maxpooling reduces this further to (24/2, 24/2) = (12, 12)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 12, 12)
    
    layer0 = ConvMaxLayer(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(96, 3, 3, 3),
    )

    layer1 = ConvMaxLayer(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 96, 16, 16),
        filter_shape=(192, 96, 3, 3),
    )

    layer2 = ConvMaxLayer(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 192, 8, 8),
        filter_shape=(192, 192, 3, 3),
        poolsize = (2,2),
    )

    output1 = layer2.output.flatten(2)

    layer3 = MaxoutLayer(
        rng,
        input = output1,
        n_in = 192 * 4 * 4,
        n_out = 500,
        maxoutsize = 5,
    )
    
    # classify the values of the fully-connected sigmoidal layer
    layer4 = LogisticRegression_2(input=layer3.output, n_in=500, n_out=10)

    # the cost we minimize during training is the NLL of the model
    #cost = layer15.negative_log_likelihood(y)
    cost = layer4.negative_log_likelihood(y) 
    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)

    # the cost we minimize during training is the NLL of the model
   
    # create a list of all model parameters to be fit by gradient descent
    
    
    params = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params
    
    #params = layer0.params + layer1.params + layer3.params + layer4.params + layer6.params + layer8.params + layer9.params + layer11.params + layer12.params + output2.params + layer13.params + layer14.params + layer15.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = Adam(params,grads,learning_rate=learning_rate)
    #updates = gradient_updates_momentum(params, cost, learning_rate = learning_rate, batch_size = batch_size, momentum = 0.5)
    #updates = Adam(params,grads,learning_rate=learning_rate)
    train_set_x_temp = T.matrix('train_set_x_temp')
    train_model = theano.function(
        [index,train_set_x_temp],
        cost,
        updates=updates,
        givens={
            x: train_set_x_temp,
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )


    test_model = theano.function(
        [index],
        layer4.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        layer4.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )


    get_hint = theano.function(
        [index],
        layer1.output,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size]
        }
    )

    get_hint_valid = theano.function(
        [index],
        layer1.output,
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size]
        }
    )

    get_hint_test = theano.function(
        [index],
        layer1.output,
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size]
        }
    )

    get_out = theano.function(
        [index],
        layer4.p_y_given_x_2,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size]
        }
    )
    
     # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model(minibatch_index,lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size])
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)

    res = []
    for minibatch_index in range(n_train_batches):
        res.append(get_hint(minibatch_index))

    res_valid = []
    for minibatch_index in range(n_valid_batches):
        res_valid.append(get_hint_valid(minibatch_index))

    '''
    res_test = []
    for minibatch_index in range(n_test_batches):
        res_test.append(get_hint_test(minibatch_index))
    '''
    teacherout = []
    for minibatch_index in range(n_train_batches):
        teacherout.append(get_out(minibatch_index))

    f = open('hint.save', 'wb')
    cPickle.dump(res, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()

    f = open('output.save', 'wb')
    cPickle.dump(teacherout, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()

    f = open('hint_valid.save', 'wb')
    cPickle.dump(res_valid, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()

    '''
    f = open('hint_test.save', 'wb')
    cPickle.dump(res_test, f, protocol=cPickle.HIGHEST_PROTOCOL)
    f.close()
    '''
    return res, teacherout

def MY_fitnet_4(datasets, teacher, teacher_valid, teacher_output, learning_rate=0.00005,n_epochs_1 = 1, n_epochs_2=128, batch_size = 64,verbose=True,decay_rate = 0.9, L2_rate = 0.00002):
    
    rng = numpy.random.RandomState(23455) 
    train_set = shared_dataset(datasets[0])
    valid_set = shared_dataset(datasets[1])
    test_set = shared_dataset(datasets[2])
    la = 4.0
    
    
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch


    x = T.matrix('x')   # the data is presented as rasterized images
    hint = T.tensor4('hint')
    teacherout = T.matrix('teacherout')
    y = T.ivector('y')
    l = T.fscalar('l')
    lad = T.fscalar('lad')
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (28-5+1 , 28-5+1) = (24, 24)
    # maxpooling reduces this further to (24/2, 24/2) = (12, 12)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 12, 12)
    
    layer0 = ConvMaxLayer_nopooling(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(32, 3, 3, 3),
    )

    layer1 = ConvMaxLayer_nopooling(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )

    layer2 = ConvMaxLayer_nopooling(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )

    layer3 = ConvMaxLayer_nopooling(
        rng,
        input=layer2.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(48, 32, 3, 3),
    )

    layer4 = ConvMaxLayer_nopooling(
        rng,
        input=layer3.output,
        image_shape=(batch_size, 48, 32, 32),
        filter_shape=(48, 48, 3, 3),
    )


    pooled_out1 = pool.pool_2d(
            input=layer4.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer5 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out1,
        image_shape=(batch_size, 48, 16, 16),
        filter_shape=(80, 48, 3, 3),
    )

    layer6 = ConvMaxLayer_nopooling(
        rng,
        input=layer5.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80, 80, 3, 3),
    )

    layer7 = ConvMaxLayer_nopooling(
        rng,
        input=layer6.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80,80, 3, 3),
    )

    layer8 = ConvMaxLayer_nopooling(
        rng,
        input=layer7.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80, 80, 3, 3),
    )

    layer9 = ConvMaxLayer_nopooling(
        rng,
        input=layer8.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80,80, 3, 3),
    )

    layer10 = ConvMaxLayer_nopooling(
        rng,
        input=layer9.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80,80, 3, 3),
    )


    layer10_compare = LeNetMaxConvLayer(
        rng,
        input = layer10.output,
        image_shape=(batch_size, 80,16,16),
        filter_shape=(192,80,9,9),
    )

    pooled_out2 = pool.pool_2d(
            input=layer10.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer11 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out2,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(128, 80, 8, 8),
    )

    layer12 = ConvMaxLayer_nopooling(
        rng,
        input=layer11.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer13 = ConvMaxLayer_nopooling(
        rng,
        input=layer12.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer14 = ConvMaxLayer_nopooling(
        rng,
        input=layer13.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer15 = ConvMaxLayer_nopooling(
        rng,
        input=layer14.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer16 = ConvMaxLayer_nopooling(
        rng,
        input=layer15.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    pooled_out3 = pool.pool_2d(
            input=layer16.output,
            ds=(8,8),
            ignore_border=True,
    )
    output1 = pooled_out3.flatten(2)
    
    layer17 = MaxoutLayer(
        rng,
        input = output1,
        n_in = 128,
        n_out = 128,
        maxoutsize = 5,
    )
    # classify the values of the fully-connected sigmoidal layer
    layer18 = LogisticRegression_2(input=layer17.output, n_in=128, n_out=10)

    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)


    cost_stage2 = layer18.negative_log_likelihood(y) + lad * T.mean(T.nnet.categorical_crossentropy(layer18.p_y_given_x_2, teacherout))
    cost_stage1 = ((hint - layer10_compare.output) ** 2).sum() * 0.5 / 50000
    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)
      
    params_stage2 = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer6.params + layer7.params + layer8.params + layer9.params + layer10.params + layer11.params + layer12.params + layer13.params + layer14.params + layer15.params + layer16.params + layer17.params + layer18.params
    
    params_stage1 = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer6.params + layer7.params + layer8.params + layer9.params + layer10.params + layer10_compare.params

    #params = layer0.params + layer1.params + layer3.params + layer4.params + layer6.params + layer8.params + layer9.params + layer11.params + layer12.params + output2.params + layer13.params + layer14.params + layer15.params

    # create a list of gradients for all model parameters
    grads_stage2 = T.grad(cost_stage2, params_stage2)
    grads_stage1 = T.grad(cost_stage1, params_stage1)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates_stage1 = Adam(params_stage1,grads_stage1,learning_rate=learning_rate)
    updates_stage2 = Adam(params_stage2,grads_stage2,learning_rate=learning_rate)
    '''
    updates_stage1 = [
        (param_i, param_i - learning_rate * grad_i)
        for param_i, grad_i in zip(params_stage1, grads_stage1)
    ]

    updates_stage2 = [
        (param_i, param_i - learning_rate * grad_i)
        for param_i, grad_i in zip(params_stage2, grads_stage2)
    ]
    '''
    #updates = gradient_updates_momentum(params, cost, learning_rate = learning_rate, batch_size = batch_size, momentum = 0.5)
    #updates = Adam(params,grads,learning_rate=learning_rate)
    train_set_x_temp = T.matrix('train_set_x_temp')
    teacher_layer = T.tensor4('teacher_layer')
    teacher_layer_valid = T.tensor4('teacher_layer_valid')
    teacher_layer_test = T.tensor4('teacher_layer_test')
    teacher_output_layer = T.matrix('teacher_output_layer')

    train_model_stage1 = theano.function(
        [train_set_x_temp,teacher_layer],
        cost_stage1,
        updates=updates_stage1,
        givens={
            x: train_set_x_temp,
            hint: teacher_layer
        }
    )

    '''
    test_model_stage1 = theano.function(
        [index,teacher_layer_test],
        cost_stage1,
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            hint: teacher_layer_test
        }
    )
    '''
    validate_model_stage1 = theano.function(
        [index,teacher_layer_valid],
        cost_stage1,
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            hint: teacher_layer_valid
        }
    )


    train_model_stage2 = theano.function(
        [index, train_set_x_temp,teacher_output_layer,l],
        cost_stage2,
        updates=updates_stage2,
        givens={
            x: train_set_x_temp,
            teacherout: teacher_output_layer,
            lad: l,
            y: train_set_y[index * batch_size: (index + 1) * batch_size],

        }
    )


    test_model_stage2 = theano.function(
        [index],
        layer18.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model_stage2 = theano.function(
        [index],
        layer18.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    #####
    ##  stage1 ####
     # early-stopping parameters
    n_epochs = n_epochs_1
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model_stage1(lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size],teacher[minibatch_index])
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model_stage1(i,teacher_valid[i]) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter
                    '''
                    # test it on the test set
                    test_losses = [
                        test_model_stage1(i,teacher_test[i])
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))
                    '''
            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process 1 for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)


    #####
    ##  stage2 ####
     # early-stopping parameters
    n_epochs = n_epochs_2
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        la = la - 0.03
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model_stage2(minibatch_index,lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size],teacher_output[minibatch_index],la)
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model_stage2(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model_stage2(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process 2 for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)
    # the cost we minimize during training is the NLL of the model
   
    # create a list of all model parameters to be fit by gradient descent
    
def MY_fitnet_1_highway(datasets, learning_rate=0.00005, n_epochs=128, batch_size = 64,verbose=True,decay_rate = 0.9, L2_rate = 0.00002):
    
    rng = numpy.random.RandomState(23455) 
    train_set = shared_dataset(datasets[0])
    valid_set = shared_dataset(datasets[1])
    test_set = shared_dataset(datasets[2])
    
    
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    # start-snippet-1
    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (28-5+1 , 28-5+1) = (24, 24)
    # maxpooling reduces this further to (24/2, 24/2) = (12, 12)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 12, 12)
    
    layer0 = ConvMaxLayer_nopooling(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(16, 3, 3, 3),
    )

    layer1 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 16, 32, 32),
        filter_shape=(16, 16, 3, 3),
    )

    layer2 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 16, 32, 32),
        filter_shape=(16, 16, 3, 3),
    )

    pooled_out1 = pool.pool_2d(
            input=layer2.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer3 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out1,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(32, 16, 3, 3),
    )

    layer4 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer3.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32, 32, 3, 3),
    )

    layer5 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer4.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32,32, 3, 3),
    )

    pooled_out2 = pool.pool_2d(
            input=layer5.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer6 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out2,
        image_shape=(batch_size, 32, 8, 8),
        filter_shape=(48, 32, 8, 8),
    )

    layer7 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer6.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(48, 48, 3, 3),
    )

    layer8 = ConvMaxLayer_nopooling(
        rng,
        input=layer7.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(64, 48, 3, 3),
    )


    pooled_out3 = pool.pool_2d(
            input=layer8.output,
            ds=(8,8),
            ignore_border=True,
    )
    output1 = pooled_out3.flatten(2)
    
    layer9 = MaxoutLayer(
        rng,
        input = output1,
        n_in = 64,
        n_out = 64,
        maxoutsize = 5,
    )
    # classify the values of the fully-connected sigmoidal layer
    layer10 = LogisticRegression(input=layer9.output, n_in=64, n_out=10)

    # the cost we minimize during training is the NLL of the model
    #cost = layer15.negative_log_likelihood(y)
    cost = layer10.negative_log_likelihood(y) 
    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)

    # the cost we minimize during training is the NLL of the model
   
    # create a list of all model parameters to be fit by gradient descent
    
    
    params = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer6.params + layer7.params + layer8.params + layer9.params + layer10.params
    
    #params = layer0.params + layer1.params + layer3.params + layer4.params + layer6.params + layer8.params + layer9.params + layer11.params + layer12.params + output2.params + layer13.params + layer14.params + layer15.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = Adam(params,grads,learning_rate=learning_rate)
    #updates = gradient_updates_momentum(params, cost, learning_rate = learning_rate, batch_size = batch_size, momentum = 0.5)
    #updates = Adam(params,grads,learning_rate=learning_rate)
    train_set_x_temp = T.matrix('train_set_x_temp')
    train_model = theano.function(
        [index,train_set_x_temp],
        cost,
        updates=updates,
        givens={
            x: train_set_x_temp,
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )


    test_model = theano.function(
        [index],
        layer10.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        layer10.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    
     # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model(minibatch_index,lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size])
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)

def MY_fitnet_1(datasets, teacher, teacher_valid, teacher_output,learning_rate=0.00005, n_epochs_1 = 10, n_epochs_2=128, batch_size = 32,verbose=True,decay_rate = 0.9, L2_rate = 0.00002):
    
    rng = numpy.random.RandomState(23455) 
    train_set = shared_dataset(datasets[0])
    valid_set = shared_dataset(datasets[1])
    test_set = shared_dataset(datasets[2])
    la = 4.0
    
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    # start-snippet-1
    x = T.matrix('x')   # the data is presented as rasterized images
    hint = T.tensor4('hint')
    teacherout = T.matrix('teacherout')
    y = T.ivector('y')
    l = T.fscalar('l')
    lad = T.fscalar('lad')
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (28-5+1 , 28-5+1) = (24, 24)
    # maxpooling reduces this further to (24/2, 24/2) = (12, 12)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 12, 12)
    
    layer0 = ConvMaxLayer_nopooling(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(16, 3, 3, 3),
    )

    layer1 = ConvMaxLayer_nopooling(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 16, 32, 32),
        filter_shape=(16, 16, 3, 3),
    )

    layer2 = ConvMaxLayer_nopooling(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 16, 32, 32),
        filter_shape=(16, 16, 3, 3),
    )

    pooled_out1 = pool.pool_2d(
            input=layer2.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer3 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out1,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(32, 16, 3, 3),
    )

    layer4 = ConvMaxLayer_nopooling(
        rng,
        input=layer3.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32, 32, 3, 3),
    )

    layer5 = ConvMaxLayer_nopooling(
        rng,
        input=layer4.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32,32, 3, 3),
    )

    layer5_compare = LeNetMaxConvLayer(
        rng,
        input = layer5.output,
        image_shape=(batch_size, 32,16,16),
        filter_shape=(192,32,9,9),
    )

    pooled_out2 = pool.pool_2d(
            input=layer5.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer6 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out2,
        image_shape=(batch_size, 32, 8, 8),
        filter_shape=(48, 32, 8, 8),
    )

    layer7 = ConvMaxLayer_nopooling(
        rng,
        input=layer6.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(48, 48, 3, 3),
    )

    layer8 = ConvMaxLayer_nopooling(
        rng,
        input=layer7.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(64, 48, 3, 3),
    )


    pooled_out3 = pool.pool_2d(
            input=layer8.output,
            ds=(8,8),
            ignore_border=True,
    )
    output1 = pooled_out3.flatten(2)
    
    layer9 = MaxoutLayer(
        rng,
        input = output1,
        n_in = 64,
        n_out = 64,
        maxoutsize = 5,
    )
    # classify the values of the fully-connected sigmoidal layer
    layer10 = LogisticRegression_2(input=layer9.output, n_in=64, n_out=10)

    # the cost we minimize during training is the NLL of the model
    #cost = layer15.negative_log_likelihood(y)
    cost_stage2 = layer10.negative_log_likelihood(y) + lad * T.mean(T.nnet.categorical_crossentropy(layer10.p_y_given_x_2, teacherout))
    cost_stage1 = ((hint - layer5_compare.output) ** 2).sum() * 0.5 / 5000
    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)
        
    params_stage2 = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer6.params + layer7.params + layer8.params + layer9.params + layer10.params
    
    params_stage1 = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer5_compare.params

    #params = layer0.params + layer1.params + layer3.params + layer4.params + layer6.params + layer8.params + layer9.params + layer11.params + layer12.params + output2.params + layer13.params + layer14.params + layer15.params

    # create a list of gradients for all model parameters
    grads_stage2 = T.grad(cost_stage2, params_stage2)
    grads_stage1 = T.grad(cost_stage1, params_stage1)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates_stage1 = Adam(params_stage1,grads_stage1,learning_rate=learning_rate)
    updates_stage2 = Adam(params_stage2,grads_stage2,learning_rate=learning_rate)
    #updates = gradient_updates_momentum(params, cost, learning_rate = learning_rate, batch_size = batch_size, momentum = 0.5)
    #updates = Adam(params,grads,learning_rate=learning_rate)
    train_set_x_temp = T.matrix('train_set_x_temp')
    teacher_layer = T.tensor4('teacher_layer')
    teacher_layer_valid = T.tensor4('teacher_layer_valid')
    teacher_layer_test = T.tensor4('teacher_layer_test')
    teacher_output_layer = T.matrix('teacher_output_layer')

    train_model_stage1 = theano.function(
        [train_set_x_temp,teacher_layer],
        cost_stage1,
        updates=updates_stage1,
        givens={
            x: train_set_x_temp,
            hint: teacher_layer
        }
    )

    validate_model_stage1 = theano.function(
        [index,teacher_layer_valid],
        cost_stage1,
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            hint: teacher_layer_valid
        }
    )


    train_model_stage2 = theano.function(
        [index, train_set_x_temp,teacher_output_layer,l],
        cost_stage2,
        updates=updates_stage2,
        givens={
            x: train_set_x_temp,
            teacherout: teacher_output_layer,
            lad: l,
            y: train_set_y[index * batch_size: (index + 1) * batch_size],

        }
    )


    test_model_stage2 = theano.function(
        [index],
        layer10.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model_stage2 = theano.function(
        [index],
        layer10.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    #####
    ##  stage1 ####
     # early-stopping parameters
    n_epochs = n_epochs_1
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model_stage1(lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size],teacher[minibatch_index])
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model_stage1(i,teacher_valid[i]) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter
                    
            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process 1 for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)


    #####
    ##  stage2 ####
     # early-stopping parameters
    n_epochs = n_epochs_2
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        la = la - 0.03
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model_stage2(minibatch_index,lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size],teacher_output[minibatch_index],la)
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model_stage2(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model_stage2(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process 2 for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)

def MY_fitnet_4_highway(datasets, learning_rate=0.00005, n_epochs=128, batch_size = 64,verbose=True,decay_rate = 0.9, L2_rate = 0.00002):
    
    rng = numpy.random.RandomState(23455) 
    train_set = shared_dataset(datasets[0])
    valid_set = shared_dataset(datasets[1])
    test_set = shared_dataset(datasets[2])
    
    
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    # start-snippet-1
    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (28-5+1 , 28-5+1) = (24, 24)
    # maxpooling reduces this further to (24/2, 24/2) = (12, 12)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 12, 12)
    
    layer0 = ConvMaxLayer_nopooling(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(32, 3, 3, 3),
    )

    layer1 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )

    layer2 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )

    layer3 = ConvMaxLayer_nopooling(
        rng,
        input=layer2.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(48, 32, 3, 3),
    )

    layer4 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer3.output,
        image_shape=(batch_size, 48, 32, 32),
        filter_shape=(48, 48, 3, 3),
    )


    pooled_out1 = pool.pool_2d(
            input=layer4.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer5 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out1,
        image_shape=(batch_size, 48, 16, 16),
        filter_shape=(80, 48, 3, 3),
    )

    layer6 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer5.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80, 80, 3, 3),
    )

    layer7 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer6.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80,80, 3, 3),
    )

    layer8 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer7.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80, 80, 3, 3),
    )

    layer9 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer8.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80,80, 3, 3),
    )

    layer10 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer9.output,
        image_shape=(batch_size, 80, 16, 16),
        filter_shape=(80,80, 3, 3),
    )


    pooled_out2 = pool.pool_2d(
            input=layer10.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer11 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out2,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(128, 80, 8, 8),
    )

    layer12 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer11.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer13 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer12.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer14 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer13.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer15 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer14.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer16 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer15.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    pooled_out3 = pool.pool_2d(
            input=layer16.output,
            ds=(8,8),
            ignore_border=True,
    )
    output1 = pooled_out3.flatten(2)
    
    layer17 = MaxoutLayer(
        rng,
        input = output1,
        n_in = 128,
        n_out = 128,
        maxoutsize = 5,
    )
    # classify the values of the fully-connected sigmoidal layer
    layer18 = LogisticRegression(input=layer17.output, n_in=128, n_out=10)

    # the cost we minimize during training is the NLL of the model
    #cost = layer15.negative_log_likelihood(y)
    cost = layer18.negative_log_likelihood(y) 
    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)

    # the cost we minimize during training is the NLL of the model
   
    # create a list of all model parameters to be fit by gradient descent
    
    
    params = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer6.params + layer7.params + layer8.params + layer9.params + layer10.params + layer11.params + layer12.params + layer13.params + layer14.params + layer15.params + layer16.params + layer17.params + layer18.params
    
    #params = layer0.params + layer1.params + layer3.params + layer4.params + layer6.params + layer8.params + layer9.params + layer11.params + layer12.params + output2.params + layer13.params + layer14.params + layer15.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = Adam(params,grads,learning_rate=learning_rate)
    #updates = gradient_updates_momentum(params, cost, learning_rate = learning_rate, batch_size = batch_size, momentum = 0.5)
    #updates = Adam(params,grads,learning_rate=learning_rate)
    train_set_x_temp = T.matrix('train_set_x_temp')
    train_model = theano.function(
        [index,train_set_x_temp],
        cost,
        updates=updates,
        givens={
            x: train_set_x_temp,
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )


    test_model = theano.function(
        [index],
        layer18.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        layer18.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    
     # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model(minibatch_index,lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size])
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)

def MY_19_highway(datasets, learning_rate=0.00005, n_epochs=128, batch_size = 64,verbose=True,decay_rate = 0.9, L2_rate = 0.00002):
    
    rng = numpy.random.RandomState(23455) 
    train_set = shared_dataset(datasets[0])
    valid_set = shared_dataset(datasets[1])
    test_set = shared_dataset(datasets[2])
    
    
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    # start-snippet-1
    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (28-5+1 , 28-5+1) = (24, 24)
    # maxpooling reduces this further to (24/2, 24/2) = (12, 12)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 12, 12)
    
    layer0 = ConvMaxLayer_nopooling(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(32, 3, 3, 3),
    )

    layer1 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )

    layer2 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )

    layer3 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer2.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )

    layer4 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer3.output,
        image_shape=(batch_size, 32, 32, 32),
        filter_shape=(32, 32, 3, 3),
    )


    pooled_out1 = pool.pool_2d(
            input=layer4.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer5 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out1,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(64, 32, 3, 3),
    )

    layer6 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer5.output,
        image_shape=(batch_size, 64, 16, 16),
        filter_shape=(64, 64, 3, 3),
    )

    layer7 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer6.output,
        image_shape=(batch_size, 64, 16, 16),
        filter_shape=(64,64, 3, 3),
    )

    layer8 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer7.output,
        image_shape=(batch_size, 64, 16, 16),
        filter_shape=(64, 64, 3, 3),
    )

    layer9 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer8.output,
        image_shape=(batch_size, 64, 16, 16),
        filter_shape=(64,64, 3, 3),
    )

    layer10 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer9.output,
        image_shape=(batch_size, 64, 16, 16),
        filter_shape=(64,64, 3, 3),
    )


    pooled_out2 = pool.pool_2d(
            input=layer10.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer11 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out2,
        image_shape=(batch_size, 64, 8, 8),
        filter_shape=(80, 64, 8, 8),
    )

    layer12 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer11.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(80, 80, 3, 3),
    )

    layer13 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer12.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(80, 80, 3, 3),
    )

    layer14 = ConvMaxLayer_nopooling(
        rng,
        input=layer13.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(128, 80, 3, 3),
    )

    layer15 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer14.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    layer16 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer15.output,
        image_shape=(batch_size, 128, 8, 8),
        filter_shape=(128, 128, 3, 3),
    )

    pooled_out3 = pool.pool_2d(
            input=layer16.output,
            ds=(8,8),
            ignore_border=True,
    )
    output1 = pooled_out3.flatten(2)
    
    layer17 = MaxoutLayer(
        rng,
        input = output1,
        n_in = 128,
        n_out = 128,
        maxoutsize = 5,
    )
    # classify the values of the fully-connected sigmoidal layer
    layer18 = LogisticRegression(input=layer17.output, n_in=128, n_out=10)

    # the cost we minimize during training is the NLL of the model
    #cost = layer15.negative_log_likelihood(y)
    cost = layer18.negative_log_likelihood(y) 
    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)

    # the cost we minimize during training is the NLL of the model
   
    # create a list of all model parameters to be fit by gradient descent
    
    
    params = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer6.params + layer7.params + layer8.params + layer9.params + layer10.params + layer11.params + layer12.params + layer13.params + layer14.params + layer15.params + layer16.params + layer17.params + layer18.params
    
    #params = layer0.params + layer1.params + layer3.params + layer4.params + layer6.params + layer8.params + layer9.params + layer11.params + layer12.params + output2.params + layer13.params + layer14.params + layer15.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = Adam(params,grads,learning_rate=learning_rate)
    #updates = gradient_updates_momentum(params, cost, learning_rate = learning_rate, batch_size = batch_size, momentum = 0.5)
    #updates = Adam(params,grads,learning_rate=learning_rate)
    train_set_x_temp = T.matrix('train_set_x_temp')
    train_model = theano.function(
        [index,train_set_x_temp],
        cost,
        updates=updates,
        givens={
            x: train_set_x_temp,
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )


    test_model = theano.function(
        [index],
        layer18.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        layer18.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    
     # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model(minibatch_index,lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size])
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)


def MY_32_highway(datasets, learning_rate=0.00005, n_epochs=128, batch_size = 64,verbose=True,decay_rate = 0.9, L2_rate = 0.00002):
    
    rng = numpy.random.RandomState(23455) 
    train_set = shared_dataset(datasets[0])
    valid_set = shared_dataset(datasets[1])
    test_set = shared_dataset(datasets[2])
    
    
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
    
    n_train_batches = train_set_x.get_value(borrow=True).shape[0] // batch_size
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0] // batch_size
    n_test_batches = test_set_x.get_value(borrow=True).shape[0] // batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch

    # start-snippet-1
    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    print('... building the model')

    layer0_input = x.reshape((batch_size, 3, 32, 32))

    # Construct the first convolutional pooling layer:
    # filtering reduces the image size to (28-5+1 , 28-5+1) = (24, 24)
    # maxpooling reduces this further to (24/2, 24/2) = (12, 12)
    # 4D output tensor is thus of shape (batch_size, nkerns[0], 12, 12)
    
    layer0 = ConvMaxLayer_nopooling(
        rng,
        input=layer0_input,
        image_shape=(batch_size, 3, 32, 32),
        filter_shape=(8, 3, 3, 3),
    )

    layer1 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer0.output,
        image_shape=(batch_size, 8, 32, 32),
        filter_shape=(8, 8, 3, 3),
    )

    layer2 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer1.output,
        image_shape=(batch_size, 8, 32, 32),
        filter_shape=(8, 8, 3, 3),
    )

    layer3 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer2.output,
        image_shape=(batch_size, 8, 32, 32),
        filter_shape=(8, 8, 3, 3),
    )

    layer4 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer3.output,
        image_shape=(batch_size, 8, 32, 32),
        filter_shape=(8, 8, 3, 3),
    )


    pooled_out1 = pool.pool_2d(
            input=layer4.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer5 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out1,
        image_shape=(batch_size, 8, 16, 16),
        filter_shape=(16, 8, 3, 3),
    )

    layer6 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer5.output,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(16, 16, 3, 3),
    )

    layer7 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer6.output,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(16,16, 3, 3),
    )

    layer8 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer7.output,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(16, 16, 3, 3),
    )

    layer9 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer8.output,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(16,16, 3, 3),
    )

    layer10 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer9.output,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(16,16, 3, 3),
    )

    layer11 = ConvMaxLayer_nopooling(
        rng,
        input=layer10.output,
        image_shape=(batch_size, 16, 16, 16),
        filter_shape=(32, 16, 3, 3),
    )

    layer12 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer11.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32, 32, 3, 3),
    )

    layer13 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer12.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32, 32, 3, 3),
    )

    layer14 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer13.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32, 32, 3, 3),
    )

    layer15 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer14.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32, 32, 3, 3),
    )

    layer16 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer15.output,
        image_shape=(batch_size, 32, 16, 16),
        filter_shape=(32, 32, 3, 3),
    )

    pooled_out2 = pool.pool_2d(
            input=layer16.output,
            ds=(2,2),
            ignore_border=True,
    )

    layer17 = ConvMaxLayer_nopooling(
        rng,
        input=pooled_out2,
        image_shape=(batch_size, 32, 8, 8),
        filter_shape=(48, 32, 3, 3),
    )

    layer18 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer17.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(48, 48, 3, 3),
    )

    layer19 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer18.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(48, 48, 3, 3),
    )

    layer20 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer19.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(48, 48, 3, 3),
    )

    layer21 = ConvMaxLayer_nopooling(
        rng,
        input=layer20.output,
        image_shape=(batch_size, 48, 8, 8),
        filter_shape=(64, 48, 3, 3),
    )

    layer22 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer21.output,
        image_shape=(batch_size, 64, 8, 8),
        filter_shape=(64, 64, 3, 3),
    )

    layer23 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer22.output,
        image_shape=(batch_size, 64, 8, 8),
        filter_shape=(64, 64, 3, 3),
    )

    layer24 = ConvMaxLayer_nopooling(
        rng,
        input=layer23.output,
        image_shape=(batch_size, 64, 8, 8),
        filter_shape=(80, 64, 3, 3),
    )

    layer25 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer24.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(80, 80, 3, 3),
    )

    layer26 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer25.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(80, 80, 3, 3),
    )

    layer27 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer26.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(80, 80, 3, 3),
    )

    layer28 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer27.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(80, 80, 3, 3),
    )

    layer29 = ConvMaxLayer_nopooling_highway(
        rng,
        input=layer28.output,
        image_shape=(batch_size, 80, 8, 8),
        filter_shape=(80, 80, 3, 3),
    )

    pooled_out3 = pool.pool_2d(
            input=layer29.output,
            ds=(8,8),
            ignore_border=True,
    )

    output1 = pooled_out3.flatten(2)
    
    layer30 = MaxoutLayer(
        rng,
        input = output1,
        n_in = 80,
        n_out = 80,
        maxoutsize = 5,
    )
    # classify the values of the fully-connected sigmoidal layer
    layer31 = LogisticRegression(input=layer30.output, n_in=80, n_out=10)

    # the cost we minimize during training is the NLL of the model
    #cost = layer15.negative_log_likelihood(y)
    cost = layer31.negative_log_likelihood(y) 
    #cost = layer15.negative_log_likelihood(y) + L2_rate * (layer0.L2_sqr + layer1.L2_sqr + layer3.L2_sqr + layer4.L2_sqr + layer6.L2_sqr + layer8.L2_sqr + layer9.L2_sqr + layer11.L2_sqr + layer12.L2_sqr + output2.L2_sqr + layer13.L2_sqr + layer14.L2_sqr)

    # the cost we minimize during training is the NLL of the model
   
    # create a list of all model parameters to be fit by gradient descent
    
    
    params = layer0.params + layer1.params + layer2.params + layer3.params + layer4.params + layer5.params + layer6.params + layer7.params + layer8.params + layer9.params + layer10.params + layer11.params + layer12.params + layer13.params + layer14.params + layer15.params + layer16.params + layer17.params + layer18.params
    params = params + layer19.params + layer20.params + layer21.params + layer22.params + layer23.params + layer24.params + layer25.params + layer26.params + layer27.params + layer28.params + layer29.params + layer30.params + layer31.params

    #params = layer0.params + layer1.params + layer3.params + layer4.params + layer6.params + layer8.params + layer9.params + layer11.params + layer12.params + output2.params + layer13.params + layer14.params + layer15.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = Adam(params,grads,learning_rate=learning_rate)
    #updates = gradient_updates_momentum(params, cost, learning_rate = learning_rate, batch_size = batch_size, momentum = 0.5)
    #updates = Adam(params,grads,learning_rate=learning_rate)
    train_set_x_temp = T.matrix('train_set_x_temp')
    train_model = theano.function(
        [index,train_set_x_temp],
        cost,
        updates=updates,
        givens={
            x: train_set_x_temp,
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )


    test_model = theano.function(
        [index],
        layer31.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    validate_model = theano.function(
        [index],
        layer31.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    
     # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = numpy.inf
    best_iter = 0
    test_score = 0.
    start_time = timeit.default_timer()

    epoch = 0
    done_looping = False
    validation_frequency = min(n_train_batches, patience // 2)
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        
        lst = []
        for i in range(datasets[0][0].shape[0]):
            x = random.randint(0,1)
            if x == 1:
                lst.append(numpy.reshape((flip_image(datasets[0][0][i])),3072))
            else:
                lst.append(numpy.copy(datasets[0][0][i]))
        
        lst = numpy.array(lst,dtype = 'float32')
        for minibatch_index in range(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if (iter % 100 == 0) and verbose:
                print('training @ iter = ', iter)
            cost_ij = train_model(minibatch_index,lst[minibatch_index * batch_size:(minibatch_index + 1) * batch_size])
            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)

                if verbose:
                    print('epoch %i, minibatch %i/%i, validation error %f %%' %
                        (epoch,
                         minibatch_index + 1,
                         n_train_batches,
                         this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)

                    if verbose:
                        print(('     epoch %i, minibatch %i/%i, test error of '
                               'best model %f %%') %
                              (epoch, minibatch_index + 1,
                               n_train_batches,
                               test_score * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = timeit.default_timer()

    # Retrieve the name of function who invokes train_nn() (caller's name)
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print(('The training process for function ' +
           calframe[1][3] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)