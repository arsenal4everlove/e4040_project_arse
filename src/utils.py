from __future__ import print_function

__docformat__ = 'restructedtext en'

import six.moves.cPickle as pickle
import gzip
import os
import sys
import timeit
import tarfile
import scipy.io
import numpy 
import glob
import urllib

import theano
import theano.tensor as T


def load_data(dataset):
    ''' Loads the dataset
    :type dataset: string
    :param dataset: the path to the dataset (here MNIST)
    '''

    #############
    # LOAD DATA #
    #############

    # Download the MNIST dataset if it is not present
    data_dir, data_file = os.path.split(dataset)
    if data_dir == "" and not os.path.isfile(dataset):
        # Check if dataset is in the data directory.
        new_path = os.path.join(
            os.path.split(__file__)[0],
            "..",
            "data",
            dataset
        )
        if os.path.isfile(new_path) or data_file == 'mnist.pkl.gz':
            dataset = new_path

    if (not os.path.isfile(dataset)) and data_file == 'mnist.pkl.gz':
        from six.moves import urllib
        origin = (
            'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
        )
        print('Downloading data from %s' % origin)
        urllib.request.urlretrieve(origin, dataset)

    print('... loading data')

    # Load the dataset
    with gzip.open(dataset, 'rb') as f:
        try:
            train_set, valid_set, test_set = pickle.load(f, encoding='latin1')
        except:
            train_set, valid_set, test_set = pickle.load(f)
    # train_set, valid_set, test_set format: tuple(input, target)
    # input is a numpy.ndarray of 2 dimensions (a matrix)
    # where each row corresponds to an example. target is a
    # numpy.ndarray of 1 dimension (vector) that has the same length as
    # the number of rows in the input. It should give the target
    # to the example with the same index in the input.

    def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables
        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        data_x, data_y = data_xy
        shared_x = theano.shared(numpy.asarray(data_x,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(numpy.asarray(data_y,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')

    test_set_x, test_set_y = shared_dataset(test_set)
    valid_set_x, valid_set_y = shared_dataset(valid_set)
    train_set_x, train_set_y = shared_dataset(train_set)

    rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    return rval

def shared_dataset(data_xy, borrow=True):
        """ Function that loads the dataset into shared variables
        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        data_x, data_y = data_xy
        shared_x = theano.shared(numpy.asarray(data_x,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        shared_y = theano.shared(numpy.asarray(data_y,
                                               dtype=theano.config.floatX),
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')
  
def one_hot(x, n):  
    """ 
    convert index representation to one-hot representation 
    """  
    x = numpy.array(x)  
    assert x.ndim == 1  
    return numpy.eye(n)[x]  
    
  
def _grayscale(a):
    print (a.reshape(a.shape[0], 3, 32, 32).mean(1).reshape(a.shape[0], -1)) 
    return a.reshape(a.shape[0], 3, 32, 32).mean(1).reshape(a.shape[0], -1)
  
def _load_batch_cifar100(filename, dtype='float64'):  
    """ 
    load a batch in the CIFAR-100 format 
    """  
    data_dir = "../data"  
    #data_dir_cifar10 = os.path.join(data_dir, "cifar-10-batches-py")  
    data_dir_cifar100 = os.path.join(data_dir, "cifar-100-python")  
    path = os.path.join(data_dir_cifar100, filename)  
    batch = numpy.load(path)  
    data = batch['data'] / 255.0  
    labels = batch['fine_labels']
    return data.astype(dtype), numpy.array(labels).astype(dtype)  
  
  
def cifar100(dtype='float64', grayscale=True):  
    x_train, t_train = _load_batch_cifar100("train", dtype=dtype)  
    x_test, t_test = _load_batch_cifar100("test", dtype=dtype)  
  
    if grayscale:  
        x_train = _grayscale(x_train)  
        x_test = _grayscale(x_test)  
  
    return x_train, t_train, x_test, t_test 

def load_data_cifar(ds_rate=None,theano_shared=False):
    data_dir = "../data"  
    #data_dir_cifar10 = os.path.join(data_dir, "cifar-10-batches-py")  
    data_dir_cifar100 = os.path.join(data_dir, "cifar-100-python.tar.gz")  
  
    #class_names_cifar10 = np.load(os.path.join(data_dir_cifar10, "batches.meta"))  
    #class_names_cifar100 = numpy.load(os.path.join(data_dir_cifar100, "meta"))  

    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    if not os.path.exists(data_dir_cifar100):
        print ("Downloading...")
        urllib.urlretrieve("http://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz", "../data/cifar-100-python.tar.gz")

    print ("Extracting...")
    os.system("tar xzvf ../data/cifar-100-python.tar.gz -C ../data")

    print ("done.")

    x_train1, y_train1, x_test1, y_test1 = cifar100(grayscale=False)
    
    train_set = (x_train1, y_train1)
    test_set = (x_test1, y_test1)
    train_set_len = len(train_set[1])

    if ds_rate is not None:
        train_set_len = int(train_set_len // ds_rate)
        train_set = [x[:train_set_len] for x in train_set]

    # Extract validation dataset from train dataset
    valid_set = [x[-(train_set_len//5):] for x in train_set]
    train_set = [x[:-(train_set_len//5)] for x in train_set]

    if theano_shared:
        train_set_x, train_set_y = shared_dataset(train_set)
        valid_set_x, valid_set_y = shared_dataset(valid_set)
        test_set_x, test_set_y = shared_dataset(test_set)

        rval = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]
    else:
        rval = [train_set, valid_set, test_set]
    
    return rval