from __future__ import print_function

__docformat__ = 'restructedtext en'

import random
import os
import sys
import timeit
import numpy
import scipy
import theano
import theano.tensor as T
#import update
from update import nesterov_momentum
from theano.tensor.nnet import relu
from theano.tensor.signal import downsample
from theano.tensor.signal import pool
from theano.tensor.nnet import conv2d
from theano.tensor.nnet.abstract_conv import bilinear_upsampling

import math

from ut import shared_dataset, load_data
from con_nn import LogisticRegression, HiddenLayer, momentum, CvLayer
#from HW3_2d import add_noise, add_noise_image_set
#Problem 2.2
#Write a function to add flip
def flip_image(image):
    choice = random.choice((1,0))
    if choice == 1:
        image = numpy.fliplr(numpy.reshape(image,(3,32,32)).transpose(1,2,0))
    else:
        image = numpy.reshape(image,(3,32,32)).transpose(1,2,0)
    return image
    
    
#Implement a convolutional neural network with the translation method for augmentation
def flip_image_set(image):
    x_axe = image[0].get_value(borrow = True).shape[0]
    y_axe = image[0].get_value(borrow = True).shape[1]
    augment_data = numpy.zeros((x_axe,y_axe))
    image_data = image[0].get_value(borrow = True)

    for i in range(x_axe):
        augment_data[i] = flip_image(image_data[i]).transpose(1,2,0).flatten("F")

    image_data = numpy.vstack((image_data,augment_data,augment_data))
    image_y = numpy.concatenate((image[1].eval(),image[1].eval(),image[1].eval()))

    # shuffle data 
    arr = numpy.arange(x_axe*3)
    numpy.random.shuffle(arr)
    image_data = image_data[arr]
    image_y = image_y[arr]

    #share data 
    train_set_x, train_set_y = shared_dataset((image_data,image_y))

    return train_set_x, train_set_y

def residual_net(n_epochs = 200, batch_size = 500, learning_rate = 0.1):
    

    rng = numpy.random.RandomState(666)
    
    # load data
    datasets = load_data(ds_rate=None,theano_shared=True)
    train_set_x, train_set_y = datasets[0]
    valid_set_x, valid_set_y = datasets[1]
    test_set_x, test_set_y = datasets[2]

    # number of minibatches for training, validation and testing
    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]
    n_train_batches //= batch_size
    n_valid_batches //= batch_size
    n_test_batches //= batch_size


    # index for a minibatch
    index = T.lscalar()
    
    x = T.matrix('x')  
    y = T.ivector('y')
    
    l_r = T.scalar('l_r',dtype=theano.config.floatX)
    #mom = T.scalar('mom', dtype=theano.config.floatX)
    
    
    ######################
    # BUILD ACTUAL MODEL #
    ######################
    
    print('... building the model')
    
    # reshape the original data into a 4d tensor
    LA0_input = x.reshape((batch_size, 3, 32, 32))
    
    
    #print(LA0_input.get_value(borrow=True).shape)
    # first conv 16*32*32
    LA1 = CvLayer(
        rng,
        input = LA0_input,
        image_shape = (batch_size,3,32,32),
        filter_shape = (16,3,3,3),
        border_mode = 'half',
        activation = relu,
        batch_norm = True,
        poolsize=(1,1)
    )
    
    print('111')

    LA2_1 = CvLayer(
        rng,
        input = LA1.output,
        image_shape = (batch_size,16,32,32),
        filter_shape = (16,16,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA2_2 = CvLayer(
        rng,
        input = LA2_1.output ,
        image_shape = (batch_size,16,32,32),
        filter_shape = (16,16,3,3),
        border_mode = 'half',
        activation = relu,
        batch_norm = True,
        poolsize = (1,1)
    )

    LA2_3 = CvLayer(
        rng,
        input = LA2_2.output + LA1.output,
        image_shape = (batch_size,16,32,32),
        filter_shape = (16,16,3,3),
        border_mode = 'half',
        activation = relu,
        batch_norm = True,
        poolsize = (1,1)
    )

    LA2_4 = CvLayer(
        rng,
        input = LA2_3.output,
        image_shape = (batch_size,16,32,32),
        filter_shape = (16,16,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA2_5 = CvLayer(
        rng,
        input = LA2_3.output + LA2_2.output,
        image_shape = (batch_size,16,32,32),
        filter_shape = (16,16,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA2_6 = CvLayer(
        rng,
        input = LA2_5.output,
        image_shape = (batch_size,16,32,32),
        filter_shape = (16,16,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )


    # second conv 32*16*16

    LA3_1 = CvLayer(
        rng,
        input = LA2_6.output + LA2_4.output,
        image_shape = (batch_size,16,32,32),
        filter_shape = (32,16,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (2,2),
        batch_norm = True
    )


    LA3_2 = CvLayer(
        rng,
        input = LA3_1.output,
        image_shape = (batch_size,32,16,16),
        filter_shape = (32,32,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    # first dimension increase layer 
    LA_increase_1 = CvLayer(
        rng,
        input = LA2_6.output,
        image_shape = (batch_size,16,32,32),
        filter_shape = (32,16,1,1),
        border_mode = 'half',
        activation = relu,
        poolsize = (2,2),
        batch_norm = False
    )


    LA3_3 = CvLayer(
        rng,
        input = LA3_2.output + LA_increase_1.output,
        image_shape = (batch_size,32,16,16),
        filter_shape = (32,32,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA3_4 = CvLayer(
        rng,
        input = LA3_3.output,
        image_shape = (batch_size,32,16,16),
        filter_shape = (32,32,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA3_5 = CvLayer(
        rng,
        input = LA3_4.output + LA3_2.output,
        image_shape = (batch_size,32,16,16),
        filter_shape = (32,32,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA3_6 = CvLayer(
        rng,
        input = LA3_5.output,
        image_shape = (batch_size,32,16,16),
        filter_shape = (32,32,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    # thrid conv 64*8*8

    LA4_1 = CvLayer(
        rng,
        input = LA3_6.output + LA3_4.output,
        image_shape = (batch_size,32,16,16),
        filter_shape = (64,32,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (2,2),
        batch_norm = True
    )

    LA4_2 = CvLayer(
        rng,
        input = LA4_1.output,
        image_shape = (batch_size,64,8,8),
        filter_shape = (64,64,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    # second dimension increase layer 
    LA_increase_2 = CvLayer(
        rng,
        input = LA3_6.output,
        image_shape = (batch_size,32,16,16),
        filter_shape = (64,32,1,1),
        border_mode = 'half',
        activation = relu,
        poolsize = (2,2),
        batch_norm = True
    )

    LA4_3 = CvLayer(
        rng,
        input = LA4_2.output + LA_increase_2.output,
        image_shape = (batch_size,64,8,8),
        filter_shape = (64,64,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA4_4 = CvLayer(
        rng,
        input = LA4_3.output,
        image_shape = (batch_size,64,8,8),
        filter_shape = (64,64,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA4_5 = CvLayer(
        rng,
        input = LA4_4.output + LA4_2.output,
        image_shape = (batch_size,64,8,8),
        filter_shape = (64,64,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    LA4_6 = CvLayer(
        rng,
        input = LA4_5.output,
        image_shape = (batch_size,64,8,8),
        filter_shape = (64,64,3,3),
        border_mode = 'half',
        activation = relu,
        poolsize = (1,1),
        batch_norm = True
    )

    # global avarege pooling 

    global_average_pooled = pool.pool_2d(
        input=LA4_6.output,
        ds=(8, 8),
        ignore_border=True,
        mode = 'average_exc_pad'
    )

    fully_input = global_average_pooled.flatten(2)

    # 10 way fully connected layer

    fully_10 = HiddenLayer(
        rng,
        input = fully_input,
        n_in = 64,
        n_out = 10,
        activation = relu
    )

    # softmax 
    y_new = LogisticRegression(fully_10.output, n_in = 10, n_out = 10)

#    L2_loss = (LA1.W ** 2).sum() + (LA2_1.W ** 2).sum() + (LA2_2.W ** 2).sum() + (LA2_3.W ** 2).sum() + (LA2_4.W ** 2).sum() + (LA2_5.W ** 2).sum() + (LA2_6.W ** 2).sum() + (LA3_1.W ** 2).sum() + (LA3_2.W ** 2).sum() + (LA3_3.W ** 2).sum()+ (LA3_4.W ** 2).sum()+ (LA3_5.W ** 2).sum() + (LA3_6.W ** 2).sum() + (LA4_1.W ** 2).sum() + (LA4_2.W ** 2).sum() + (LA4_3.W ** 2).sum() + (LA4_4.W ** 2).sum()+ (LA4_5.W ** 2).sum() + (LA4_6.W ** 2).sum()+ (fully_10.W ** 2).sum() + (LA_increase_1.W ** 2).sum()+ (LA_increase_2.W ** 2).sum()


    cost = y_new.negative_log_likelihood(y) 

    
    
    # test and validate model 
    
    test_model = theano.function(
        [index],
        y_new.errors(y),
        givens={
            x: test_set_x[index*batch_size: (index+1)*batch_size],
            y: test_set_y[index*batch_size: (index+1)*batch_size]
        }
    )
    
    validate_model = theano.function(
        [index],
        y_new.errors(y),
        givens={
            x: valid_set_x[index*batch_size: (index+1)*batch_size],
            y: valid_set_y[index*batch_size: (index+1)*batch_size]
        }
    )
    
    # a list of all parameters to be optimized by the gds
    
    params = LA1.params + LA2_1.params + LA2_2.params + LA2_3.params + LA2_4.params + LA2_5.params + LA2_6.params + LA3_1.params + LA3_2.params + LA3_3.params+LA3_4.params + LA3_5.params + LA3_6.params + LA4_1.params + LA4_2.params + LA4_3.params + LA4_4.params + LA4_5.params + LA4_6.params + fully_10.params + LA_increase_1.params+LA_increase_2.params
    
    # a list of gradients for all model parameters
    #grads = T.grad(cost, params)
    
    #updates = momentum(params, grads, learning_rate = 0.001, momentum = 0.9)
    
    """
    updates = []
    for p, g in zip(params, grads):
        v = p.get_value(borrow = True)
        velocity = theano.shared(numpy.zeros(v.shape, dtype = v.dtype), broadcastable = p.broadcastable)
        x = 0.9 * velocity - l_r * g
        updates.append((velocity, x))
        updates.append((p, p + x))
    """
    gparams = [T.grad(cost, param) for param in params]
    updates = nesterov_momentum(params, gparams, learning_rate = l_r, momentum = 0.9)   
    
    # train model 
    train_model = theano.function(
        inputs = [index,l_r],
        outputs = cost,
        updates = updates,
        givens = {
            x: train_set_x[index*batch_size: (index + 1) * batch_size],
            y: train_set_y[index*batch_size: (index + 1) * batch_size]
            
        }
    )
    
    ###############
    # TRAIN MODEL #
    ###############
    
    print('...training')
    # early-stopping parameters
    patience = 100000 # check at least this many examples 
    patience_increase = 2 # wait this much time before new best is found
    improvement_threshold = 0.995 # this much improvement is significant
    validation_frequency = min(n_train_batches,patience//2) # go through this much epoch before checking on the validation set; here we check every epoch
    
    best_validation_loss = numpy.inf
    best_iter = 0
    best_score =0.
    start_time = timeit.default_timer()
    
    epoch = 0
    done_looping = False
    
    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1 
        for minibatch_index in range(n_train_batches):
            
            
            iter = (epoch - 1) * n_train_batches + minibatch_index
            
            if iter == 32000:
                learning_rate = learning_rate / 10
            if iter == 48000:
                learning_rate = learning_rate / 10
            if iter == 64000:
                print('stop now')
            if iter % 100 == 0:
                print('training @ iter = ', iter)
                
            minibatch_avg_cost = train_model(minibatch_index,learning_rate)
            
            if (iter+1) % validation_frequency == 0:
                # computer zero-one loss on validation set 
                validation_losses = [validate_model(i) for i in range(n_valid_batches)]
                this_validation_loss = numpy.mean(validation_losses)
                print('epoch %i, minibatch %i/%i, validation error %f %%' %
                      (epoch,minibatch_index + 1, n_train_batches, this_validation_loss*100.))
                
                # if we get the best validation score so far
                if this_validation_loss < best_validation_loss:
                    
                    # improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)
                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter
                    
                    # test it on the test set 
                    test_losses = [
                        test_model(i)
                        for i in range(n_test_batches)
                    ]
                    test_score = numpy.mean(test_losses)
                    print(('     epoch %i, minibatch %i/%i, test error of '
                           'best model %f %%') %
                          (epoch, minibatch_index + 1, n_train_batches,
                           test_score * 100.))
                    
            if patience <= iter:
                done_looping = True
                break
                
    end_time = timeit.default_timer()
    print('Optimization complete.')
    print('Best validation score of %f %% obtained at iteration %i, '
          'with test performance %f %%' %
          (best_validation_loss * 100., best_iter + 1, test_score * 100.))
    print(('The code for file ' +
           os.path.split("__file__")[1] +
           ' ran for %.2fm' % ((end_time - start_time) / 60.)), file=sys.stderr)
    return test_score