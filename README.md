# FINAL PROJECT FOR ECBM4040 NEURAL NETWRKS & DEEP LEARNING

## Group Index:  ARSE

### Group member: 
* **Ruixiong Shi  rs3569**
* **Yiqun    Nian yn2289**
* **Yuchen   Shi  ys2901**
			  

### Highway Network:
* mnist_depth{10,20,50,100}.ipynb: Reproducing Figure 1 in paper 'Highway Network' 
* depth50_visual.ipynb: Reproducing Figure 2 in paper 'Highway Network' 

#### realted doc: 
* utils.py, fuctions loading dataset
* update.py, functions for optimization algorithm
* mnist.py, building highway network for mnist_depth{10,20,50,100}.ipynb
* visual.py, building highway network plus some inspection function for depth50_visual.ipynb


### Fitnet:
* fitnet_1.ipynb: Training teacher network, save the outputs that we need and then train fitnet1
* fitnet_4.ipynb: Training fitnet4
* highway1.ipynb: Traininng Highway1(compared with fitnet1) reproducing Table 1 in paper 'Highway Network'
* highway2.ipynb: Training Highway2(compared with fitnet4) reproducing Table 1 in paper 'Highway Network'
* highway3.ipynb: Training Highway3 reproducing Table 1 in paper 'Highway Network'

#### related doc:  
* finalPro.py, functions used to train all of the fitnets and highway networks
* hw3_nn.py, basic functions for neural networks(which we used in hw3)
* hw3_utils.py, basic functions used to load dataset(CIFAR-10)


### Residual Net:
* con_nn.py: Building blocks of residual network
* updates.py: Various training strategy could be used such adam
* ut.py: load data and data preprossessing 
* residual_net.py: Building residual network
* resnet.ipynb: Training residual net


### Reference
**Highway Network**
[Authors: R. K. Srivastava, K. Greff, and J. Schmidhuber. Highway networks. arXiv:1505.00387, 2015.](https://arxiv.org/pdf/1505.00387v2.pdf)

**Fitnet**
[Authors: Romero, Adriana, Ballas, Nicolas, Kahou, Samira Ebrahimi, Chassang, Antoine, Gatta, Carlo, and Bengio, Yoshua. FitNets: Hints for thin deep nets. arXiv:1412.6550 [cs], December 2014.](http://arxiv.org/abs/1412.6550.)

**Residual Net**
[Authors: Kaiming He and Xiangyu Zhang and Shaoqing Ren and Jian Sun. Deep Residual Learning for Image Recognition. arXiv preprint arXiv:1512.03385, 2015](https://arxiv.org/pdf/1512.03385.pdf)
