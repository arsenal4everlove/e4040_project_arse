# FINAL PROJCT FOR ECBM4040 NEURAL NETWRKS & DEEP LEARNING

## Group Index:  ARSE

### Group member: 
* **Ruixiong Shi  rs3569**
* **Yiqun    Nian yn2289**
* **Yuchen   Shi  ys2901**
			  

### Highway Network:
* mnist_depth{10,20,50,100}.ipynb: Reproducing Figure 1 in paper 'Highway Network' 
* depth50_visual.ipynb: Reproducing Figure 2 in paper 'Highway Network' 

* realted doc: 
* utils.py, fuctions loading dataset
* update.py, functions for optimization algorithm
* mnist.py, building highway network for mnist_depth{10,20,50,100}.ipynb
* visual.py, building highway network plus some inspection function for depth50_visual.ipynb

*Authors: R. K. Srivastava, K. Greff, and J. Schmidhuber. Highway networks. arXiv:1505.00387, 2015. 
URL https://arxiv.org/pdf/1505.00387v2.pdf


### Fitnet:

### Residual Net:


